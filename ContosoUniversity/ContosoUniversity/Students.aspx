﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Students.aspx.cs" Inherits="ContosoUniversity.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h2>Student List</h2>
<p>
    <asp:ValidationSummary ID="StudentsValidationSummary" runat="server" ShowSummary="true" 
        DisplayMode="BulletList" Style="color: Red" />
    <asp:GridView ID="StudentsGridView" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="PersonID" 
        DataSourceID="StudentsEntityDataSource">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:TemplateField HeaderText="Name" SortExpression="LastName"> 
                <EditItemTemplate> 
                    <%--<asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>'></asp:TextBox> 
                    <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstMidName") %>'></asp:TextBox>--%>
                    <asp:DynamicControl ID="LastNameTextBox" runat="server" DataField="LastName" Mode="Edit" /> 
                    <asp:DynamicControl ID="FirstNameTextBox" runat="server" DataField="FirstMidName" Mode="Edit" />
                </EditItemTemplate> 
                <ItemTemplate> 
                    <%--<asp:Label ID="LastNameLabel" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>, 
                    <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Eval("FirstMidName") %>'></asp:Label>--%>
                    <asp:DynamicControl ID="LastNameLabel" runat="server" DataField="LastName" Mode="ReadOnly" />, 
                    <asp:DynamicControl ID="FirstNameLabel" runat="server" DataField="FirstMidName" Mode="ReadOnly" />
                </ItemTemplate> 
            </asp:TemplateField>
            <asp:DynamicField DataField="EnrollmentDate" HeaderText="Enrollment Date" SortExpression="EnrollmentDate" />
            <%--<asp:TemplateField HeaderText="Enrollment Date" SortExpression="EnrollmentDate"> 
                <EditItemTemplate> 
                    <asp:TextBox ID="EnrollmentDateTextBox" runat="server" Text='<%# Bind("EnrollmentDate", "{0:d}") %>'></asp:TextBox> 
                </EditItemTemplate> 
                <ItemTemplate> 
                    <asp:Label ID="EnrollmentDateLabel" runat="server" Text='<%# Eval("EnrollmentDate", "{0:d}") %>'></asp:Label> 
                </ItemTemplate> 
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="Number of Courses"> 
                <ItemTemplate> 
                    <asp:Label ID="NumberOfCoursesLabel" runat="server" Text='<%# Eval("StudentGrades.Count") %>'></asp:Label> 
                </ItemTemplate> 
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</p>
    <asp:EntityDataSource ID="StudentsEntityDataSource" runat="server" 
        ContextTypeName="ContosoUniversity.DAL.SchoolEntities" 
        EnableDelete="True" EnableFlattening="False" EnableUpdate="True" 
        EntitySetName="People" EntityTypeFilter="Student" Include="StudentGrades"
        OrderBy="it.LastName">
    </asp:EntityDataSource>

<h2>Find Students by Name</h2> 
<p>
    Enter any part of the name 
    <asp:TextBox ID="SearchTextBox" runat="server" AutoPostBack="true"></asp:TextBox> 
    &nbsp;<asp:Button ID="SearchButton" runat="server" Text="Search" /> 
</p> 
<p>
    <asp:GridView ID="SearchGridView" runat="server" AutoGenerateColumns="False" DataKeyNames="PersonID" 
        DataSourceID="SearchEntityDataSource" AllowPaging="true"> 
        <Columns> 
            <asp:TemplateField HeaderText="Name" SortExpression="LastName, FirstMidName"> 
                <ItemTemplate> 
                    <%--<asp:Label ID="LastNameFoundLabel" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>,  
                    <asp:Label ID="FirstNameFoundLabel" runat="server" Text='<%# Eval("FirstMidName") %>'></asp:Label>--%>
                    <asp:DynamicControl ID="LastNameLabel" runat="server" DataField="LastName" Mode="ReadOnly" />, 
                    <asp:DynamicControl ID="FirstNameLabel" runat="server" DataField="FirstMidName" Mode="ReadOnly" />
                </ItemTemplate> 
            </asp:TemplateField>
            <asp:DynamicField DataField="EnrollmentDate" HeaderText="Enrollment Date" SortExpression="EnrollmentDate" />
            <%--<asp:TemplateField HeaderText="Enrollment Date" SortExpression="EnrollmentDate"> 
                <ItemTemplate> 
                    <asp:Label ID="EnrollmentDateFoundLabel" runat="server" Text='<%# Eval("EnrollmentDate", "{0:d}") %>'></asp:Label> 
                </ItemTemplate> 
            </asp:TemplateField>--%> 
        </Columns> 
    </asp:GridView>
</p>
    <asp:EntityDataSource ID="SearchEntityDataSource" runat="server"  
        ContextTypeName="ContosoUniversity.DAL.SchoolEntities" EnableFlattening="False"  
        EntitySetName="People" EntityTypeFilter="Student" 
        
        Where="it.FirstMidName Like '%' + @StudentName + '%' or it.LastName Like '%' + @StudentName + '%'"> 
        <WhereParameters> 
            <asp:ControlParameter ControlID="SearchTextBox" Name="StudentName" PropertyName="Text"  
             Type="String" DefaultValue="%"/> 
        </WhereParameters> 
    </asp:EntityDataSource>
</asp:Content>
